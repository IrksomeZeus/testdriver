#include "test.h"

#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>
#include <linux/compiler.h>
#include <linux/device.h>
#include <linux/export.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/irqreturn.h>
#include <linux/kern_levels.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/semaphore.h>
#include <linux/spinlock.h>
#include <linux/spinlock_types.h>
#include <linux/stddef.h>
#include <linux/types.h>
#include <uapi/asm-generic/errno-base.h>
#include <uapi/asm-generic/int-ll64.h>

ssize_t my_read(struct file *, char *, size_t, loff_t *);
ssize_t my_write(struct file *, const char *, size_t, loff_t *);
long my_ioctl(struct file *, unsigned int, unsigned long);
int my_open(struct inode *, struct file *);
int my_release(struct inode *, struct file *);

#define VERSION "1.1"
#define DEVICE_NAME "led_blinker"
#define NUM_DEVICES (1)
#define NUM_OF_REGISTERS (6)
#define MEM_SIZE (32)
#define LW_BASE_PHYSICAL_ADDR (0xFF200000)
#define LED_PHYSICAL_ADDR (0x00000000 + LW_BASE_PHYSICAL_ADDR)
#define FIFO_DATA_ADDR (vfDeviceVirtAddr + 4)
#define FIFO_COUNT_ADDR (vfDeviceVirtAddr + 5)
#define FIFO_SIZE (256)
#define LED_IRQ (72)

static dev_t mydev;
static __u32* vfDeviceVirtAddr;

struct cdev my_cdev;
struct semaphore irq_sem;
struct class* cl;
spinlock_t vfFifoProtection;

struct file_operations my_fops =
    {
    .owner = THIS_MODULE,
    .read = my_read,
    .write = my_write,
    .unlocked_ioctl = my_ioctl,
    .open = my_open,
    .release = my_release,
    };

int FIFO_count(void)
    {
    int count;
    count = ioread32(FIFO_COUNT_ADDR);
    return count;
    }

int FIFO_freespace(void)
    {
    int freeSpace;
    freeSpace = (FIFO_SIZE - FIFO_count());
    return freeSpace;
    }

int FIFO_isEmpty(void)
    {
    int isEmpty;
    isEmpty = (FIFO_count() == 0);
    return isEmpty;
    }

static irqreturn_t FIFO_interrupt(int irq, void* dev_id)
    {
    up(&irq_sem);
    return IRQ_HANDLED;
    }

ssize_t my_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
    {
    int i;
    char tempBuf[FIFO_SIZE];
    static int check4Eof = 0;

    if (check4Eof)
        {
        if (FIFO_isEmpty())
            {
            check4Eof = 0;
            return 0;
            }
        }
    while (FIFO_count() == 0)
        {
        if (down_interruptible(&irq_sem) != 0)
            {
            return 0;
            }
        }
    if (FIFO_isEmpty())
        {
        return 0;
        }
    if (FIFO_count() < count)
        {
        count = FIFO_count();
        }
    spin_lock(&vfFifoProtection);
    for (i = 0; i < count; i++)
        {
        tempBuf[i] = ioread32(FIFO_DATA_ADDR);
        }
    spin_unlock(&vfFifoProtection);
    tempBuf[count] = 0;
    if (copy_to_user(buf, &tempBuf, count) != 0)
        {
        printk(KERN_WARNING "Failed to copy read data\n");
        return -1;
        }
    // after sending user data, we need to force the read routine to check for EOF
    // and return EOF if found the next time it is called.
    check4Eof = 1;
    return count;
    }

ssize_t my_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
    {

    int i;
    char tempBuf[FIFO_SIZE];

    if (count >= FIFO_SIZE)
        {
        printk(KERN_WARNING "Invalid write count\n");
        return -1;
        }

    if (count > FIFO_freespace())
        {
        count = FIFO_freespace();
        }

    if (copy_from_user(&tempBuf, buf, count) != 0)
        {
        printk(KERN_WARNING "Failed to copy write data\n");
        return -1;
        }
    spin_lock(&vfFifoProtection);
    for (i = 0; i < count; i++)
        {
        iowrite32(tempBuf[i], FIFO_DATA_ADDR);
        }
    spin_unlock(&vfFifoProtection);
    return count;
    }

long my_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
    {
    __u32 ReadResult;
    ioctl_cmd_t* pIoctlCmd = (ioctl_cmd_t*) &ioctl_num;
    __u16 cmd = pIoctlCmd->cmd;
    __u16 offset = pIoctlCmd->offset;
    __u32 address;
    if (offset >= NUM_OF_REGISTERS)
        {
        printk(KERN_ERR "Offset should be less than %d; You entered %d.\n", NUM_OF_REGISTERS, offset);
        return -1;
        }
    address = (__u32 ) (vfDeviceVirtAddr + offset);
    switch (cmd)
        {
    case DRIVER_IOWR:
        iowrite32(ioctl_param, (void* ) (address));
        break;
    case DRIVER_IORD:
        ReadResult = ioread32((void* ) (address));
        if (copy_to_user((char *) ioctl_param, &ReadResult, 4) != 0)
            {
            return -1;
            }
        break;
        }
    return 0;
    }

int my_open(struct inode *inode, struct file *file)
    {
    return 0;
    }

int my_release(struct inode *inode, struct file *file)
    {
    return 0;
    }

static int __init ledBlinker_init(void)
    {
    struct resource* ResourcePtr;
    printk(KERN_INFO "Loading %s module version %s.\n", DEVICE_NAME, VERSION);

    sema_init(&irq_sem, 0);
    spin_lock_init(&vfFifoProtection);

    if (alloc_chrdev_region(&mydev, 0, NUM_DEVICES, DEVICE_NAME) < 0)
        {
        printk(KERN_ERR "Can't register device\n");
        return -1;
        }

    if ((cl = class_create(THIS_MODULE, DEVICE_NAME)) == NULL)
        {
        printk(KERN_ERR "Unable to create class. Aborting.\n");
        unregister_chrdev_region(mydev, NUM_DEVICES);
        return -1;
        }

    cdev_init(&my_cdev, &my_fops);
    my_cdev.owner = THIS_MODULE;
    if (cdev_add(&my_cdev, mydev, 1) == -1)
        {
        printk(KERN_ERR "Unable to add cdev. Aborting.\n");
        class_destroy(cl);
        unregister_chrdev_region(mydev, NUM_DEVICES);
        return -1;
        }

    if (device_create(cl, NULL, mydev, NULL, DEVICE_NAME) == NULL)
        {
        printk(KERN_ERR "Unable to create device. Aborting.\n");
        cdev_del(&my_cdev);
        class_destroy(cl);
        unregister_chrdev_region(mydev, NUM_DEVICES);
        return -1;
        }

    ResourcePtr = request_mem_region(LED_PHYSICAL_ADDR, MEM_SIZE, DEVICE_NAME);
    if (ResourcePtr == NULL)
        {
        printk(KERN_ERR "Unable to allocate memory region\n");
        return -EFAULT;
        }

    vfDeviceVirtAddr = ioremap_nocache(LED_PHYSICAL_ADDR, MEM_SIZE);
    if (vfDeviceVirtAddr == NULL)
        {
        printk(KERN_ERR "Unable to remap memory region\n");
        return -EFAULT;
        }

    if (request_irq(LED_IRQ, FIFO_interrupt, IRQF_TRIGGER_RISING, "led_irq", NULL))
        {
        printk(KERN_ERR "LED_BLINKER: Can't register IRQ %d\n", LED_IRQ);
        return -EIO;
        }
    printk(KERN_INFO "%s has acquired interrupt %d.\n", DEVICE_NAME, LED_IRQ);

    return 0;
    }

static void __exit ledBlinker_exit(void)
    {
    free_irq(LED_IRQ, NULL);

    iounmap(vfDeviceVirtAddr);

    release_mem_region(LED_PHYSICAL_ADDR, MEM_SIZE);

    device_destroy(cl, mydev);

    cdev_del(&my_cdev);

    class_destroy(cl);

    unregister_chrdev_region(mydev, NUM_DEVICES);

    printk(KERN_INFO "Module unloaded\n");
    }

module_init(ledBlinker_init);
module_exit(ledBlinker_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Austin Yates");
MODULE_DESCRIPTION("Basic FIFO Module");
MODULE_VERSION(VERSION);
