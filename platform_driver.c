#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>
#include <linux/compiler.h>
#include <linux/device.h>
#include <linux/export.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/irqreturn.h>
#include <linux/kern_levels.h>
#include <linux/mod_devicetable.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/printk.h>
#include <linux/semaphore.h>
#include <linux/spinlock.h>
#include <linux/spinlock_types.h>
#include <linux/stddef.h>
#include <linux/types.h>
#include <uapi/asm-generic/int-ll64.h>

#include "test.h"

static int driver_probe(struct platform_device *);
static int driver_remove(struct platform_device *);
ssize_t device_read(struct file *, char *, size_t, loff_t *);
ssize_t device_write(struct file *, const char *, size_t, loff_t *);
long device_ioctl(struct file *, unsigned int, unsigned long);
int device_open(struct inode *, struct file *);
int device_release(struct inode *, struct file *);

#define VERSION "1.0"
#define DEVICE_NAME "ledBlinker"
#define FIFO_DATA_ADDR (vfSimpleDriverVirtAddr + 4)
#define FIFO_COUNT_ADDR (vfSimpleDriverVirtAddr + 5)
#define FIFO_SIZE (256)
#define NUM_OF_REGISTERS (9)

static ssize_t vfSimpleDriverBaseAddr;
static int vfSimpleDriverIrqNum;
static int vfSimpleDriverSize;
static __u32* vfSimpleDriverVirtAddr;
static dev_t vfDevNum;

struct cdev vfCdev;
struct class* vfDevClass;
struct semaphore irq_sem;
spinlock_t vfFifoProtection;

static struct of_device_id simple_driver_ids[] =
    {
        {
        .compatible = "cust,led-blinker"
        },
        {/*end of table*/
        }
    };

MODULE_DEVICE_TABLE(of, simple_driver_ids);

static struct platform_driver simple_driver =
    {
    .probe = driver_probe,
    .remove = driver_remove,
    .driver =
        {
        .name = DEVICE_NAME,
        .owner = THIS_MODULE,
        .of_match_table = simple_driver_ids,
        },
    /*
     .shutdown = unused,
     .suspend = unused,
     .resume = unused,
     .id_table = unused,
     */
    };

struct file_operations f_ops =
    {
    .owner = THIS_MODULE,
    .read = device_read,
    .write = device_write,
    .unlocked_ioctl = device_ioctl,
    .open = device_open,
    .release = device_release,
    };

int FIFO_count(void)
    {
    int count;
    count = ioread32(FIFO_COUNT_ADDR);
    return count;
    }

int FIFO_freespace(void)
    {
    int freeSpace;
    freeSpace = (FIFO_SIZE - FIFO_count());
    return freeSpace;
    }

int FIFO_isEmpty(void)
    {
    int isEmpty;
    isEmpty = (FIFO_count() == 0);
    return isEmpty;
    }

ssize_t device_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
    {
    int i;
    char tempBuf[FIFO_SIZE];
    static int check4Eof = 0;

    if (check4Eof)
        {
        if (FIFO_isEmpty())
            {
            check4Eof = 0;
            return 0;
            }
        }
    while (FIFO_count() == 0)
        {
        if (down_interruptible(&irq_sem) != 0)
            {
            return 0;
            }
        }
    if (FIFO_isEmpty())
        {
        return 0;
        }
    if (FIFO_count() < count)
        {
        count = FIFO_count();
        }
    spin_lock(&vfFifoProtection);
    for (i = 0; i < count; i++)
        {
        tempBuf[i] = ioread32(FIFO_DATA_ADDR);
        }
    spin_unlock(&vfFifoProtection);
    tempBuf[count] = 0;
    if (copy_to_user(buf, &tempBuf, count) != 0)
        {
        printk(KERN_WARNING "Failed to copy read data\n");
        return -1;
        }
    // after sending user data, we need to force the read routine to check for EOF
    // and return EOF if found the next time it is called.
    check4Eof = 1;
    return count;
    }

ssize_t device_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
    {

    int i;
    char tempBuf[FIFO_SIZE];

    if (count >= FIFO_SIZE)
        {
        printk(KERN_WARNING "Invalid write count\n");
        return -1;
        }

    if (count > FIFO_freespace())
        {
        count = FIFO_freespace();
        }

    if (copy_from_user(&tempBuf, buf, count) != 0)
        {
        printk(KERN_WARNING "Failed to copy write data\n");
        return -1;
        }
    spin_lock(&vfFifoProtection);
    for (i = 0; i < count; i++)
        {
        iowrite32(tempBuf[i], FIFO_DATA_ADDR);
        }
    spin_unlock(&vfFifoProtection);
    return count;
    }

long device_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
    {
    __u32 ReadResult;
    ioctl_cmd_t* pIoctlCmd = (ioctl_cmd_t*) &ioctl_num;
    __u16 cmd = pIoctlCmd->cmd;
    __u16 offset = pIoctlCmd->offset;
    __u32 address;
    if (offset >= NUM_OF_REGISTERS)
        {
        printk(KERN_ERR "Offset should be less than %d; You entered %d.\n", NUM_OF_REGISTERS, offset);
        return -1;
        }
    address = (__u32 ) (vfSimpleDriverVirtAddr + offset);
    switch (cmd)
        {
    case DRIVER_IOWR:
        iowrite32(ioctl_param, (void* ) (address));
        break;
    case DRIVER_IORD:
        ReadResult = ioread32((void* ) (address));
        if (copy_to_user((char *) ioctl_param, &ReadResult, 4) != 0)
            {
            return -1;
            }
        break;
        }
    return 0;
    }

int device_open(struct inode *inode, struct file *file)
    {
    return 0;
    }

int device_release(struct inode *inode, struct file *file)
    {
    return 0;
    }

static irqreturn_t FIFO_interrupt(int irq, void* dev_id)
    {
    pr_info("***************FIFO INTERRUPT***************");
    up(&irq_sem);
    return IRQ_HANDLED;
    }

static int driver_probe(struct platform_device *pdev)
    {
    struct resource *driver_resource;
    pr_info("Probing...\n");
    driver_resource = platform_get_resource(pdev, IORESOURCE_MEM, 0);
    if (driver_resource == NULL)
        {
        pr_err("IORESOURCE_MEM, 0 does not exist\n");
        return -1;
        }
    vfSimpleDriverBaseAddr = driver_resource->start;
    vfSimpleDriverSize = resource_size(driver_resource);
    vfSimpleDriverIrqNum = platform_get_irq(pdev, 0);
    if (request_mem_region(vfSimpleDriverBaseAddr, vfSimpleDriverSize, "SimpleDriverMem") == NULL)
        {
        pr_err("Memory allocation failed\n");
        return -1;
        }
    vfSimpleDriverVirtAddr = ioremap_nocache(vfSimpleDriverBaseAddr, vfSimpleDriverSize);
    if (vfSimpleDriverVirtAddr == NULL)
        {
        pr_err("Memory map failed\n");
        release_mem_region(vfSimpleDriverBaseAddr, vfSimpleDriverSize);
        return -1;
        }
    if (request_irq(vfSimpleDriverIrqNum, FIFO_interrupt, IRQF_TRIGGER_RISING, "Simple_Interrupt", &simple_driver))
        {
        pr_err("Interrupt request failed\n");
        iounmap(vfSimpleDriverVirtAddr);
        release_mem_region(vfSimpleDriverBaseAddr, vfSimpleDriverSize);
        return -1;
        }
    pr_info("Success\n");
    return 0;
    }

static int driver_remove(struct platform_device *pdev)
    {
    pr_info("Remove\n");
    free_irq(vfSimpleDriverIrqNum, &simple_driver);
    iounmap(vfSimpleDriverVirtAddr);
    release_mem_region(vfSimpleDriverBaseAddr, vfSimpleDriverSize);
    return 0;
    }

static int __init simple_driver_init(void)
    {
    pr_info("Initializing %s version %s\n", DEVICE_NAME, VERSION);

    if (platform_driver_probe(&simple_driver, &driver_probe) != 0)
        {
        pr_err("ERROR: Platform Device Probe Failed\n");
        return -1;
        }

    sema_init(&irq_sem, 0);
    spin_lock_init(&vfFifoProtection);

    if (alloc_chrdev_region(&vfDevNum, 0, 1, DEVICE_NAME) < 0)
        {
        pr_err("ERROR: Device Registration Failed\n");
        platform_driver_unregister(&simple_driver);
        return -1;
        }
    if ((vfDevClass = class_create(THIS_MODULE, DEVICE_NAME)) == NULL)
        {
        pr_err("ERROR: Class Creation Failed\n");
        platform_driver_unregister(&simple_driver);
        unregister_chrdev_region(vfDevNum, 1);
        return -1;
        }
    cdev_init(&vfCdev, &f_ops);
    vfCdev.owner = THIS_MODULE;
    if (cdev_add(&vfCdev, vfDevNum, 1) == -1)
        {
        pr_err("ERROR: Device Addition Failed\n");
        platform_driver_unregister(&simple_driver);
        class_destroy(vfDevClass);
        unregister_chrdev_region(vfDevNum, 1);
        return -1;
        }
    if (device_create(vfDevClass, NULL, vfDevNum, NULL, DEVICE_NAME) == NULL)
        {
        pr_err("ERROR: Device Creation Failed\n");
        platform_driver_unregister(&simple_driver);
        cdev_del(&vfCdev);
        class_destroy(vfDevClass);
        unregister_chrdev_region(vfDevNum, 1);
        return -1;
        }

    return 0;
    }

static void __exit simple_driver_exit(void)
    {
    pr_info("Exiting %s\n", DEVICE_NAME);
    platform_driver_unregister(&simple_driver);
    device_destroy(vfDevClass, vfDevNum);
    cdev_del(&vfCdev);
    class_destroy(vfDevClass);
    unregister_chrdev_region(vfDevNum, 1);
    pr_info("Successfully Removed\n");
    }

module_init(simple_driver_init);
module_exit(simple_driver_exit);

MODULE_AUTHOR("Austin Yates");
MODULE_DESCRIPTION("Simple platform device driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(VERSION);
