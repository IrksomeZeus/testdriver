obj-m := test.o
obj-m += platform_driver.o

ifndef OUT_DIR 
$(error OUT_DIR is undefined, bad environment, you point OUT_DIR to the linux kernel build output directory)
endif

KDIR ?= $(OUT_DIR)

default:
	$(MAKE) -C $(KDIR) M=$$PWD
	sshpass -p proot scp /home/byates/testDriver/test.ko root@192.168.22.106:/root/testDriver
	sshpass -p proot scp /home/byates/testDriver/platform_driver.ko root@192.168.22.106:/root/testDriver

clean:
	$(MAKE) -C $(KDIR) M=$$PWD clean
	
help:
	$(MAKE) -C $(KDIR) M=$$PWD help
	
modules:
	$(MAKE) -C $(KDIR) M=$$PWD modules
	
modules_install:
	$(MAKE) -C $(KDIR) M=$$PWD modules_install
