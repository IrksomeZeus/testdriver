#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include "../../testDriver/test.h"

int asciiToInt(char* input)
	{
	int data;
	if(input[0] == '0' & input[1] == 'x')
		{
		data = strtol(input, NULL, 16);
		}
	else
		{
		data = atoi(input);
		}
	return data;
	}

uint32_t ioctl_read(int fileHandle, uint16_t offset)
	{
	uint32_t data;
	victory_ioctl_cmd_t command;
	command.cmd = VICTORY_IORD;
	command.offset = offset;
	int* commandAsInt;
	commandAsInt = (int *) &command;
	int rv;
	rv = ioctl(fileHandle, *commandAsInt, &data);
	if (rv != 0)
		{
		printf("Error in ioctl read = %i\n", rv);
		return 0;
		}
	return data;
	}

int ioctl_write(int fileHandle, uint16_t offset, uint32_t data)
	{
	victory_ioctl_cmd_t command;
	command.cmd = VICTORY_IOWR;
	command.offset = offset;
	int *commandAsInt;
	commandAsInt = (int *) &command;
	int rv;
	rv = ioctl(fileHandle, *commandAsInt, data);
	if (rv != 0)
		{
		printf("Error in ioctl write = %i\n", rv);
		}
	return 0;
	}

int main(int argc, char* argv[])
	{
	if (argc < 2)
		{
		printf("missing filename\n");
		return -1;
		}
	if (argc < 3)
		{
		printf("missing offset\n");
		return -1;
		}
	if (argc < 4)
		{
		printf("missing command\n");
		}
	int FileHandle;
	FileHandle = open(argv[1], O_RDWR);
	if (FileHandle < 0)
		{
		printf("unable to open file; %s\n", argv[1]);
		return -1;
		}
	uint16_t offset;
	offset = atoi(argv[2]);

	uint16_t command;
	command = atoi(argv[3]);
	switch (command)
		{
	case 0:
		if (argc < 5)
			{
			printf("No data to write\n");
			return -1;
			}
		printf("Writing\n");
		uint32_t data;
		data = asciiToInt(argv[4]);
		ioctl_write(FileHandle, offset, data);
		printf("Writing to Reg[%d] complete\n", offset);
		break;
	case 1:
		printf("Reading\n");
		uint32_t result;
		result = ioctl_read(FileHandle, offset);
		printf("Reg[%d] = 0x%x\n", offset, result);
		break;
		}
	close(FileHandle);
	return 0;
	}

