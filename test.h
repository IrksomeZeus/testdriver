#include <linux/types.h>

#define DRIVER_IOWR (0)
#define DRIVER_IORD (1)

// struct for IO control commands
typedef struct
		__attribute__((packed))
			{
			__u16 cmd;
			__u16 offset;
		} ioctl_cmd_t;
